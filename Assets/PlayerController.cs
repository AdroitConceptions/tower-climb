﻿
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public Rigidbody2D rb2d;
    public OverlapChecker groundCheck;
    [SerializeField] float jumpForce = 10f;
    [SerializeField] float motiveForce = 100f;
    bool jump = false;
    Vector2 movement;
    void Update()
    {
        Gamepad pad = Gamepad.current;
        movement = Vector2.zero;
        if (pad != null)
        {
            if (pad.buttonWest.wasPressedThisFrame)
            {
                jump = true;
            }
            movement = pad.dpad.ReadValue();
        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
            jump = true;
        }
        if (movement.sqrMagnitude < 0.1f)
        {
            movement = new Vector2(
                (Input.GetKey(KeyCode.LeftArrow) ? -1f : 0f) + (Input.GetKey(KeyCode.RightArrow) ? 1f : 0f),
                (Input.GetKey(KeyCode.DownArrow) ? -1f : 0f) + (Input.GetKey(KeyCode.UpArrow) ? 1f : 0f)
            );
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            QuitGame();
        }
    }

    void QuitGame()
    {
        Debug.Log("Quit Button was pressed");
        Application.Quit();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (jump && groundCheck.isOverlapping) Jump();
        jump = false;
        rb2d.AddForce(new Vector2(movement.x * motiveForce, 0f));
    }

    void Jump()
    {
        rb2d.AddForce(new Vector2(0f, jumpForce));
    }
}
