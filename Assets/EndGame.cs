﻿using System.Collections;
using UnityEngine;

public class EndGame : MonoBehaviour
{
    public GameObject sucessObject;

    // Start is called before the first frame update
    void Start()
    {
        sucessObject.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        sucessObject.SetActive(true);
        StartCoroutine(EndingDelay());
    }

    IEnumerator EndingDelay()
    {
        yield return new WaitForSeconds(5f);
        Debug.Log("Quitting");
        Application.Quit();
    }


}
