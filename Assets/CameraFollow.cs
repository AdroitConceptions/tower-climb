﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Bounds levelBounds;
    public Bounds camBounds;
    public float lerpSpeed = .95f;
    public float cameraYOffset = 2f;
    public Transform target;

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 targetPos = transform.position;
        targetPos.y = Mathf.Lerp(targetPos.y, target.position.y + cameraYOffset, lerpSpeed * Time.deltaTime);
        targetPos.x = Mathf.Lerp(targetPos.x, target.position.x, lerpSpeed * Time.deltaTime);

        targetPos.x = Mathf.Clamp(
            targetPos.x,
            levelBounds.center.x - (levelBounds.extents.x - camBounds.extents.x),
            levelBounds.center.x + (levelBounds.extents.x - camBounds.extents.x));
        targetPos.y = Mathf.Clamp(
            targetPos.y,
            levelBounds.center.y - (levelBounds.extents.y - camBounds.extents.y),
            levelBounds.center.y + (levelBounds.extents.y - camBounds.extents.y));
        transform.position = targetPos;
    }
}
