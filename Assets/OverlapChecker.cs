﻿using UnityEngine;

public class OverlapChecker : MonoBehaviour
{
    public bool isOverlapping { get; private set; }

    void OnTriggerEnter2D(Collider2D coll)
    {
        isOverlapping = true;
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        isOverlapping = false;
    }
}
